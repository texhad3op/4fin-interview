package com.forfinance.interview.beans;

import com.forfinance.interview.entities.Status;

import java.sql.Timestamp;


public class LoanRequestResult {

    private Status status;
    private String message;
    private String loanGuid;
    private String requestTime;

    public LoanRequestResult(final Status status, final String errorMessage, final String loanGuid,
                             final String requestTime) {
        this.status = status;
        this.message = errorMessage;
        this.loanGuid = loanGuid;
        this.requestTime = requestTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLoanGuid() {
        return loanGuid;
    }

    public void setLoanGuid(String loanGuid) {
        this.loanGuid = loanGuid;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(final String requestTime) {
        this.requestTime = requestTime;
    }
}
