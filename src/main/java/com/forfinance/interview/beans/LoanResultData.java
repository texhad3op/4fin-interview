package com.forfinance.interview.beans;

import com.forfinance.interview.entities.LoanTerm;
import com.forfinance.interview.entities.Status;

public class LoanResultData {

    private String registrationDate;

    private Long amount;

    private String uuid;

    private LoanTerm loanTerm;

    private String message;

    private Status status;

    public LoanResultData(final String registrationDate, final Long amount, final String uuid, final LoanTerm loanTerm, final String message, final Status status) {
        this.registrationDate = registrationDate;
        this.amount = amount;
        this.uuid = uuid;
        this.loanTerm = loanTerm;
        this.message = message;
        this.status = status;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LoanTerm getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(LoanTerm loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
