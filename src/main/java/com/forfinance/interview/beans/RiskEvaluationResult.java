package com.forfinance.interview.beans;

public class RiskEvaluationResult {
    public enum Conclusion {
        RISKY, SAFE
    }

    private String risksMap;
    private Conclusion state;

    public RiskEvaluationResult(final String risksMap, final Conclusion state) {
        this.risksMap = risksMap;
        this.state = state;
    }

    public String getRisksMap() {
        return risksMap;
    }

    public void setRisksMap(final String risksMap) {
        this.risksMap = risksMap;
    }

    public Conclusion getState() {
        return state;
    }

    public void setState(final Conclusion state) {
        this.state = state;
    }
}
