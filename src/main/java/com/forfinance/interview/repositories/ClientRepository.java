package com.forfinance.interview.repositories;


import com.forfinance.interview.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query("SELECT c FROM Client c where c.firstName = ?1 AND c.lastName = ?2")
    Client findClient(final String firstName, final String lastName);
}
