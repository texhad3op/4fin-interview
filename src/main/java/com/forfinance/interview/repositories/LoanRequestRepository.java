package com.forfinance.interview.repositories;

import com.forfinance.interview.entities.LoanRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LoanRequestRepository extends JpaRepository<LoanRequest, Long> {

    @Query("SELECT lr FROM LoanRequest lr where lr.client.firstName = ?1 AND lr.client.lastName = ?2 order by lr.registrationDate")
    List<LoanRequest> getClientHistory(final String firstName, final String lastName);

    @Query(value = "SELECT count(*) FROM LOAN_REQUEST as lr inner join CLIENT as c on c.ID = lr.CLIENT_ID" +
            " where  EXTRACT(DAY FROM CURRENT_DATE)||'-'||EXTRACT(MONTH FROM CURRENT_DATE)||'-'" +
            "||EXTRACT(YEAR FROM CURRENT_DATE) = EXTRACT(DAY FROM DATE)||'-'||EXTRACT(MONTH FROM DATE)||'-'||EXTRACT(YEAR FROM DATE)" +
            "AND c.FIRSTNAME = ?1 AND c.LASTNAME = ?2 and lr.ip_address = ?3", nativeQuery = true)
    int getApplicationsCount(final String firstName, final String lastName, final String ipAddress);


}

