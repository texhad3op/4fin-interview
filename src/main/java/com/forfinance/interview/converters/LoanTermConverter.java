package com.forfinance.interview.converters;

import com.forfinance.interview.entities.LoanTerm;

import java.beans.PropertyEditorSupport;


public class LoanTermConverter extends PropertyEditorSupport {

    public void setAsText(final String text) throws IllegalArgumentException {
        setValue(LoanTerm.fromValue(text));
    }
}

