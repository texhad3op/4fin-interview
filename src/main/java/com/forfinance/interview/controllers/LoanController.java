package com.forfinance.interview.controllers;

import com.forfinance.interview.beans.LoanRequestResult;
import com.forfinance.interview.beans.LoanResultData;
import com.forfinance.interview.converters.LoanTermConverter;
import com.forfinance.interview.entities.LoanTerm;
import com.forfinance.interview.services.ConverterService;
import com.forfinance.interview.services.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
public class LoanController {

    @Autowired
    LoanService loanService;

    @Autowired
    ConverterService converterService;

    @RequestMapping("/loanrequest/{firstname}/{lastname}/{amount}/{term:month|halfyear|year}")
    @ResponseBody
    public LoanRequestResult loanRequest(@PathVariable(value = "firstname") String firstName,
                                         @PathVariable(value = "lastname") String lastName, @PathVariable(value = "amount") Long amount,
                                         @PathVariable(value = "term") LoanTerm loanTerm, HttpServletRequest request) {
        return loanService.requestLoan(firstName, lastName, amount, loanTerm, request.getRemoteAddr());
    }

    @RequestMapping("/loanhistory/{firstname}/{lastname}")
    @ResponseBody
    public List<LoanResultData> getClientHistory(@PathVariable(value = "firstname") String firstName,
                                                 @PathVariable(value = "lastname") String lastName) {
        return converterService.covert(loanService.getClientHistory(firstName, lastName));
    }

    @InitBinder
    public void initBinder(final WebDataBinder webdataBinder) {
        webdataBinder.registerCustomEditor(LoanTerm.class, new LoanTermConverter());
    }

}
