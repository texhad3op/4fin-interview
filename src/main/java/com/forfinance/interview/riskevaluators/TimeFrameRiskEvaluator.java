package com.forfinance.interview.riskevaluators;

import java.sql.Timestamp;
import java.time.LocalDateTime;


public class TimeFrameRiskEvaluator implements RiskEvaluator {
    public TimeFrameRiskEvaluator(final Timestamp applyDateTime) {
        this.applyDateTime = applyDateTime.toLocalDateTime();
    }

    private LocalDateTime applyDateTime;

    @Override
    public boolean fired() {
        return applyDateTime.getHour() >= 0 && applyDateTime.getHour() <= 6;
    }
}
