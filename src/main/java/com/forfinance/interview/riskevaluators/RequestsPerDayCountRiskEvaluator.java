package com.forfinance.interview.riskevaluators;

public class RequestsPerDayCountRiskEvaluator implements RiskEvaluator {

    private Integer applicationsNumber;

    public RequestsPerDayCountRiskEvaluator(final Integer applicationsNumber) {
        this.applicationsNumber = applicationsNumber;
    }

    @Override
    public boolean fired() {
        return applicationsNumber > 2;
    }
}
