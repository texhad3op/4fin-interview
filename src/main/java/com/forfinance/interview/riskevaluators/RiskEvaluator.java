package com.forfinance.interview.riskevaluators;

public interface RiskEvaluator {
    boolean fired();
}
