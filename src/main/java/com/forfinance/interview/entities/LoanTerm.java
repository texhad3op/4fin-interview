package com.forfinance.interview.entities;

import java.util.Arrays;
import java.util.stream.Stream;


public enum LoanTerm
{
	MONTH("month"), HALF_YEAR("halfyear"), YEAR("year");

	private String value;

	LoanTerm(final String value)
	{
		this.value = value;
	}

	public static LoanTerm fromValue(String value)
	{
		return Stream.of(values()).filter(loanTerm -> loanTerm.value.equalsIgnoreCase(value)).findFirst().orElseThrow(
				() -> new IllegalArgumentException(
						"Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values())));
	}
}
