package com.forfinance.interview.entities;

public enum Status {
    APPROVED, DECLINED
}
