package com.forfinance.interview.services;

import com.forfinance.interview.beans.LoanRequestResult;
import com.forfinance.interview.entities.LoanRequest;
import com.forfinance.interview.entities.LoanTerm;

import java.util.List;

public interface LoanService {

    LoanRequestResult requestLoan(String firstName, String lastName, Long amount, LoanTerm loanTerm, String ipAddress);

    List<LoanRequest> getClientHistory(String firstName, String lastName);
}
