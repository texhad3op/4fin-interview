package com.forfinance.interview.services.impl;

import com.forfinance.interview.beans.LoanResultData;
import com.forfinance.interview.entities.LoanRequest;
import com.forfinance.interview.services.ConverterService;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConverterServiceImpl implements ConverterService {
    @Override
    public List<LoanResultData> covert(List<LoanRequest> list) {
        return list.stream().map(lr -> new LoanResultData(getDateTime(lr.getRegistrationDate()), lr.getAmount(), lr.getUuid(), lr.getLoanTerm(),lr.getMessage(), lr.getStatus() )).collect(Collectors.toList());
    }
    @Override
    public String  getDateTime(final Timestamp timestamp){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return dateFormat.format(timestamp);
    }
}
