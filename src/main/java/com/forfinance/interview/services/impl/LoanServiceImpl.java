package com.forfinance.interview.services.impl;

import com.forfinance.interview.beans.LoanRequestResult;
import com.forfinance.interview.beans.RiskEvaluationResult;
import com.forfinance.interview.entities.Client;
import com.forfinance.interview.entities.LoanRequest;
import com.forfinance.interview.entities.LoanTerm;
import com.forfinance.interview.entities.Status;
import com.forfinance.interview.repositories.ClientRepository;
import com.forfinance.interview.repositories.LoanRequestRepository;
import com.forfinance.interview.services.ConverterService;
import com.forfinance.interview.services.LoanRiskEvalutorService;
import com.forfinance.interview.services.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;


@Service
public class LoanServiceImpl implements LoanService {

    private static final String DECISION_MESSAGE_PREFIX = "decisionmessage.";
    private static final String DECISION_MESSAGE_APPROVED = DECISION_MESSAGE_PREFIX + "00";

    @Autowired
    private LoanRiskEvalutorService loanRiskEvalutorService;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private LoanRequestRepository loanRequestRepository;

    @Autowired
    private ConverterService converterService;

    @Override
    @Transactional
    public LoanRequestResult requestLoan(final String firstName, final String lastName, final Long amount, final LoanTerm loanTerm,
                                         final String ipAddress) {
        final Timestamp requestTime = new Timestamp(System.currentTimeMillis());
        final String requestTimeAsString = converterService.getDateTime(requestTime);

        final RiskEvaluationResult riskEvaluationResult = loanRiskEvalutorService
                .getLoanRiskEvaluation(firstName, lastName, ipAddress, requestTime);


        Status status = Status.DECLINED;
        String uuid = null;


        if (riskEvaluationResult.getState().equals(RiskEvaluationResult.Conclusion.SAFE)) {
            uuid = UUID.randomUUID().toString();
            status = Status.APPROVED;
        }

        String decisionMessage = getDecisionMessage(riskEvaluationResult.getRisksMap());

        saveLoanResult(firstName, lastName, amount,
                ipAddress, uuid, requestTime, status, loanTerm, decisionMessage);

        return new LoanRequestResult(status, decisionMessage, uuid,
                requestTimeAsString);
    }

    private void saveLoanResult(final String firstName, final String lastName, final Long amount,
                                final String ipAddress, final String uuid, final Timestamp requestTime, final Status status, LoanTerm loanTerm, String decisionMessage) {
        Client client = clientRepository.findClient(firstName, lastName);
        if (client == null)
            client = new Client(firstName, lastName);
        LoanRequest loanRequest = new LoanRequest(requestTime, ipAddress, uuid, client, amount, status, loanTerm, decisionMessage);
        client.getRequests().add(loanRequest);
        loanRequestRepository.save(loanRequest);
        clientRepository.save(client);
    }


    private String getDecisionMessage(final String risksMap) {
        String decisionMessage = context.getMessage(DECISION_MESSAGE_PREFIX + risksMap, null, null);
        if (decisionMessage == null)
            decisionMessage = context.getMessage(DECISION_MESSAGE_APPROVED, null, null);
        return decisionMessage;
    }

    @Override
    public List<LoanRequest> getClientHistory(final String firstName, final String lastName) {
        return loanRequestRepository.getClientHistory(firstName, lastName);
    }
}