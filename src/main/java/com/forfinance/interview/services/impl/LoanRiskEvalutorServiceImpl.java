package com.forfinance.interview.services.impl;

import com.forfinance.interview.beans.RiskEvaluationResult;
import com.forfinance.interview.repositories.LoanRequestRepository;
import com.forfinance.interview.riskevaluators.RequestsPerDayCountRiskEvaluator;
import com.forfinance.interview.riskevaluators.RiskEvaluator;
import com.forfinance.interview.riskevaluators.TimeFrameRiskEvaluator;
import com.forfinance.interview.services.LoanRiskEvalutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class LoanRiskEvalutorServiceImpl implements LoanRiskEvalutorService {

    @Autowired
    LoanRequestRepository loanRequestRepository;

    @Override
    public RiskEvaluationResult getLoanRiskEvaluation(final String firstName, final String lastName,
                                                      final String ipAddress, final Timestamp applyDateTime) {
        List<RiskEvaluator> riskEvaluators = new ArrayList<>();
        riskEvaluators.add(new TimeFrameRiskEvaluator(applyDateTime));
        riskEvaluators.add(new RequestsPerDayCountRiskEvaluator(loanRequestRepository.getApplicationsCount(firstName, lastName, ipAddress)));
        return generateRiskConclusion(riskEvaluators);
    }

    private RiskEvaluationResult generateRiskConclusion(final List<RiskEvaluator> riskEvaluators) {
        final String risksMap = riskEvaluators.stream().map(riskEvaluator -> riskEvaluator.fired())
                .map((Boolean res) -> res == true ? "1" : "0").collect(Collectors.joining());

        return new RiskEvaluationResult(risksMap, risksMap.contains("1") ?
                RiskEvaluationResult.Conclusion.RISKY :
                RiskEvaluationResult.Conclusion.SAFE);
    }

}
