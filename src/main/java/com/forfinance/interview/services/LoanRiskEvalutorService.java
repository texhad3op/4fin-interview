package com.forfinance.interview.services;

import com.forfinance.interview.beans.RiskEvaluationResult;

import java.sql.Timestamp;


public interface LoanRiskEvalutorService {
    RiskEvaluationResult getLoanRiskEvaluation(String firstName, String lastName,
                                               String ipAddress, Timestamp applyDateTime);
}
