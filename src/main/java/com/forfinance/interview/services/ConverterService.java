package com.forfinance.interview.services;

import com.forfinance.interview.beans.LoanResultData;
import com.forfinance.interview.entities.LoanRequest;

import java.sql.Timestamp;
import java.util.List;

public interface ConverterService {
    List<LoanResultData> covert(List<LoanRequest> list);

    String getDateTime(final Timestamp timestamp);
}
