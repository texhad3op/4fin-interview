package com.forfinance.interview.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LoanControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void loanPositiveTest() throws Exception {

        this.mockMvc.perform(get("/loanrequest/aaa/bbb/1000/month")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("APPROVED")
                ).andExpect(jsonPath("message").value("Your applying is approved."));

    }


    @Test
    public void loanNegativeTest() throws Exception {

        this.mockMvc.perform(
                get("/loanrequest/ccc/ddd/1000/month")
        ).andDo(print()).andExpect(status().isOk()
        );

        this.mockMvc.perform(
                get("/loanrequest/ccc/ddd/1000/month")
        ).andDo(print()).andExpect(status().isOk()
        );
        this.mockMvc.perform(
                get("/loanrequest/ccc/ddd/1000/month")
        ).andDo(print()).andExpect(status().isOk()
        );

        this.mockMvc.perform(
                get("/loanrequest/ccc/ddd/1000/month")
        ).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$.status").value("DECLINED")
        );

    }

}
