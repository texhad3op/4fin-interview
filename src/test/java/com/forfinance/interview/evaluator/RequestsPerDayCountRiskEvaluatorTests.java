package com.forfinance.interview.evaluator;

import com.forfinance.interview.riskevaluators.RequestsPerDayCountRiskEvaluator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RequestsPerDayCountRiskEvaluatorTests {

    @Test
    public void permittedRequestsPerDayCountRiskEvaluatorPositiveTest() {
        final RequestsPerDayCountRiskEvaluator requestsPerDayCountRiskEvaluator = new RequestsPerDayCountRiskEvaluator(2);
        Assert.assertEquals(false, requestsPerDayCountRiskEvaluator.fired());
    }

    @Test
    public void permittedRequestsPerDayCountRiskEvaluatorNegativeTest() {
        final RequestsPerDayCountRiskEvaluator requestsPerDayCountRiskEvaluator = new RequestsPerDayCountRiskEvaluator(4);
        Assert.assertEquals(true, requestsPerDayCountRiskEvaluator.fired());
    }

}

