package com.forfinance.interview.evaluator;

import com.forfinance.interview.riskevaluators.TimeFrameRiskEvaluator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TimeFrameRiskEvaluatorTests {


    @Test
    public void permittedTimeFrameRiskEvaluatorPositiveTest() {
        final TimeFrameRiskEvaluator timeFrameRiskEvaluator = new TimeFrameRiskEvaluator(getTime("23/09/2007 22:50"));
        Assert.assertEquals(false, timeFrameRiskEvaluator.fired());
    }

    @Test
    public void permittedTimeFrameRiskEvaluatorNegativeTest() {
        final TimeFrameRiskEvaluator timeFrameRiskEvaluator = new TimeFrameRiskEvaluator(getTime("23/09/2007 03:50"));
        Assert.assertEquals(true, timeFrameRiskEvaluator.fired());
    }


    private Timestamp getTime(final String time) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date = null;
        try {
            date = dateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Timestamp(date.getTime());
    }

}
