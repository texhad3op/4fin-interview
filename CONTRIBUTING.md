To run this project:
1. git clone https://gitlab.com/texhad3op/4fin-interview.git
2. mvn spring-boot:run
3. to request loan: http://localhost:8080/loanrequest/{name}/{surname}/(amount}/{term} <br/>
    example: http://localhost:8080/loanrequest/aaa/bbb/1000/month
4. to get client history:http://localhost:8080/loanhistory/{name}/{surname} <br/>
    example: http://localhost:8080/loanhistory/aaa/bbb/